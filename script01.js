let visitorName = prompt('Please, enter your name here (at least two symbols):');
while ((visitorName.length > 1) == false) {
    visitorName = prompt('Incorrect name! Please, enter your correct name here (at least two symbols):', [visitorName])
}

let visitorAge = prompt('Please, enter your age (full years) here:');
while ((visitorAge > 0 ) == false){
    visitorAge = prompt('Incorrect! Please, enter correct number for your age:', [visitorAge])
}
while ((visitorAge < 120 ) == false){
    visitorAge = prompt('Incorrect! Please, enter correct number for your age:', [visitorAge])
}
while ((visitorAge % 1 ) != 0){
    visitorAge = prompt('Incorrect! Please, enter correct number for your age:', [visitorAge])
}

if (visitorAge < 18) {
    alert("Sorry, you are not allowed to visit this site");
} else {
    if (visitorAge < 23) {
        if (confirm("Are you sure you want to access this site?") === true) {
            alert("Welcome," + " " + visitorName);
        } else {
            alert("You are not allowed to visit this site");
        }
    } else {
        alert("Welcome," + " " + visitorName);
    }
} 